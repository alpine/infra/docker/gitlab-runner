# gitlab-runner docker image

This will create a docker image for running a gitlab runner on Alpine Linux on
all the architectures that Alpine Linux supports.

## Profiles

At startup, it will look for profiles in /data/profiles. These are files with
variables that define how that specific runner is registered. Look at the help
output of `gitlab-runner register --help` to see what variables are supported.

Note that variables are automatically exported (so no need to use `export`).

The following variables are required:

* `GITLAB_ACCESS_TOKEN` - A personal access token that is used to create the
  runners. Must have at least the `create_runner` scope.
* `RUNNER_DESCRIPTION` - A description that shows up in the web interface for
  this runner.
* `RUNNER_TYPE` - The type of runner to create. Either `project_type`,
  `group_type` or `instance_type`.
* `RUNNER_TAGS` - The tags to give this runner to specify what jobs to run.
* `RUNNER_PARENT_ID` - The id of the parent scope. The project id for
  `project_type` runners, or group id for `group_type` runners.

The following variables have a default set, but can be overridden:

| Variable            | Default Value                              |
| ------------------- | ------------------------------------------ |
| `CI_SERVER_URL`     | https://gitlab.alpinelinux.org             |
| `DOCKER_IMAGE`      | `registry.alpinelinux.org/img/alpine:edge` |
| `RUNNER_LIMIT`      | 1                                          |
| `RUNNER_CONCURRENT` | 10                                         |

