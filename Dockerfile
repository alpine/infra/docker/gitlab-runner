FROM registry.alpinelinux.org/img/alpine:edge

ENV SSL_CERT_FILE=/etc/ssl/cert.pem

RUN apk add -U --no-cache gitlab-runner curl jq

COPY register-runners entrypoint /usr/local/bin/

VOLUME ["/etc/gitlab-runner"]
ENTRYPOINT ["/usr/local/bin/entrypoint"]
CMD ["run", "--user=gitlab-runner", "--working-directory=/var/lib/gitlab-runner"]
